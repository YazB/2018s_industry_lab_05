package ictgradschool.industry.abstraction.pets;

/**
 * Main program.
 */
public class AnimalApp {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the pets array with a Bird, a Dog and a Horse.
        animals[0] = new Dog();
        animals[1] = new Bird();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the pets in the given list, and print their details as shown in the lab handout.
        // TODO If the animal also implements IFamous, print out that corresponding info too.
        for (IAnimal aList : list) {
            System.out.println(aList.myName() + " says " + aList.sayHello());

            System.out.print(aList.myName() + " is ");
            if (aList.isMammal()) {
                System.out.println("a mammal.");
            } else {
                System.out.println("a non-mammal.");
            }

           // System.out.println(aList.myName() + " is a " + (aList.isMammal() ? "mammal" : "non-mammal")    ); // ternary, shorthand for the above if-else

            System.out.println("Did I forget to tell you I have " + aList.legCount() + " legs.");
            if (aList instanceof IFamous) {
                System.out.println("This is a famous name for my animal type: " + ((IFamous) aList).famous());
            }

            System.out.println("-------------------------------------------------------");
        }
    }

    public static void main(String[] args) {
        new AnimalApp().start();
    }
}
