package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.industry.abstraction.Supermarket;

public class Meat extends Supermarket {
    @Override
    public String getProductName() {
        return "Meaty meats for all!";
    }

    @Override
    public int getBasePrice() {
        return 10;
    }

    @Override
    public int getQuantityOfStock() {
        return 99;
    }
}
