package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.industry.abstraction.Supermarket;

public class Bakery extends Supermarket {
    @Override
    public String getProductName() {
        return "Nana's baking";
    }

    @Override
    public int getBasePrice() {
        return 4;
    }

    @Override
    public int getQuantityOfStock() {
        return 20;
    }
}
