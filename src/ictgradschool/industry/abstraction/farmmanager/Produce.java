package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.industry.abstraction.Supermarket;

public class Produce extends Supermarket {

    @Override
    public static String getProductName()
    {return "Fresh Fruit";}

    @Override
    public int getBasePrice() {
        return 3;
    }

    @Override
    public int getQuantityOfStock() {
        return 500;
    }
}
