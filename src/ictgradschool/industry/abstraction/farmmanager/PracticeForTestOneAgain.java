package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.Keyboard;

import static java.lang.Math.*;

public class PracticeForTestOneAgain {
    int n;
    String c = Keyboard.readInput();
    int d = Integer.parseInt(c);
    //Do these values change when used??
    int max;
    int min;
    int value;

    private String getUserNameOne() {
        System.out.println("Please enter the name of the first contestant:");
        String a = Keyboard.readInput();
        return a;
    }

    private String getUserNameTwo() {
        System.out.println("Please enter the name of the second contestant:");
        String b = Keyboard.readInput();
        return b;
    }

    private String getUserNameThree() {
        String c = Keyboard.readInput();
        return c;
    }

    //Check works: START FROM HERE
    private String getIntegers() {
        System.out.println("Please enter an integer number greater than 0:");
        String c = Keyboard.readInput();
        int d = Integer.parseInt(c);
        while (d < 0) {
            System.out.println("Please enter an integer greater than 0:");
            if (d >= 0) {
                System.out.println("Please enter an integer number greater than" + d + ":");
            } else continue;
        }
        while (n < d) {
            System.out.println("Please enter an integer greater than" + d + ":");
            if (n >= d) {
                continue;
            } else continue;
        }
        return null;
    }
//Check works: ENDS HERE

    private String getMaxAndMin() {
        System.out.println("The minimum radius of a pie is:" + " " + min(d, n));
        System.out.println("The maximum radius of a pie is:" + " " + max(d, n));
        //Need to store values??
        return null;
    }
    private int getRandomNumber() {
        //Need to get random number between d and n
        int range = max - min + 1;
        double randomNumber = (double) (random() * range + min);
        double circumference = (2 * Math.PI * randomNumber);
        int value = (int) circumference;
        return value;
    }
    private String returnPieSize() {
        if (value > 500) {
            return "extra large";
        } else if (value < 500 && value > 100) {
            return "large";
        } else if (value <= 100 && value > 40) {
            return "medium";
        } else if (value <= 40 && value > 10) {
            return "small";
        } else if (value <= 10) {
            return "extra small";
        }
        return null;
    }
    private String getContestantInitial(){
        //TODO
        return null;
    }
}
