package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.industry.abstraction.Supermarket;

public class Deli extends Supermarket {
    @Override
    public String getProductName() {
        return "Christmas ham";
    }

    @Override
    public int getBasePrice() {
        return 15;
    }

    @Override
    public int getQuantityOfStock() {
        return 50;
    }
}
