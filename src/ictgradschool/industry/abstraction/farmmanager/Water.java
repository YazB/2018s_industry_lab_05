package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.industry.abstraction.Supermarket;

public class Water extends Supermarket {
    @Override
    public String getProductName() {
        return "Shire's Spring Water";
    }

    @Override
    public int getBasePrice() {
        return 9;
    }

    @Override
    public int getQuantityOfStock() {
        return 250;
    }
}
