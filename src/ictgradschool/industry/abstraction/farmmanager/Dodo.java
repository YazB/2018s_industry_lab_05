package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.industry.abstraction.farmmanager.animals.Animal;

public class Dodo extends Animal{
    private final int MAX_VALUE=400;

    public Dodo()  {value=200; }

    public void feed() {
        if(value<MAX_VALUE) {
            value=value+(MAX_VALUE-value)/2;
        }
    }
    public int costToFeed() {return 5;}

    public String getType() {return "Dodo";}

    public String toString() {return getType()+ " -$ "+value;}
}

