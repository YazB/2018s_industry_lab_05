package ictgradschool.industry.abstraction.farmmanager;

import ictgradschool.industry.abstraction.Supermarket;

public class Cleaning extends Supermarket {
    @Override
    public String getProductName() {
        return "Spray and walk away!";
    }

    @Override
    public int getBasePrice() {
        return 8;
    }

    @Override
    public int getQuantityOfStock() {
        return 1500;
    }
}
