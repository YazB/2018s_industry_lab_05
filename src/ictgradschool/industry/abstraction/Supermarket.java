package ictgradschool.industry.abstraction;

import ictgradschool.industry.abstraction.farmmanager.Produce;

public abstract class Supermarket {

    public abstract String getProductName();

    public abstract int getBasePrice();

    public abstract int getQuantityOfStock();

    public void printSupermarketItems() {
        System.out.println("These are the items stored in your supermarket:");
        }
    }

    }

