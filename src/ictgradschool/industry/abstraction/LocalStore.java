package ictgradschool.industry.abstraction;

public abstract class LocalStore {

    public abstract String setSupermarketName();

    public abstract String setSupermarketLocation();

    public abstract int setMaxValueOfStock();



}
